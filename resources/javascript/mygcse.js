let pre_filter = { };

let templates = { };

$(document).ready(function( ) {
	$.get("resources/templates/file.txt", function(data) {
		templates.file = data;
		
		$.getJSON("configuration/directories.json", function(data) {
			getDirectories(data, function( ) {
				filter( );
			});
		});
	});
	
	$(document).on("click", ".card-footer .btn", function( ) {
		$(".launch-file").attr("href", "files/" + pre_filter[ $(this).closest(".card").attr("data-file") ].file);
		$(".launch-scheme").attr("href", "files/" + pre_filter[ $(this).closest(".card").attr("data-file") ].scheme);
		$("#launch iframe").attr("src", "files/" + pre_filter[ $(this).closest(".card").attr("data-file") ].file);
		$("body").css("overflow", "hidden");
		$(".nav-main").hide( );
		$("#launch").show( );
	});
	
	$(document).on("click", ".launch-close", function( ) {
		$("body").css("overflow", "auto");
		$(".nav-main").show( );
		$("#launch").hide( );
	});
	
	$(document).on("click", "#nav-documents-tab", function( ) {
		$("nav").removeClass("nav-home");
	});
	
	$(document).on("click", "#nav-home-tab", function( ) {
		$("nav").addClass("nav-home");
	});
	
	$(document).on("input", "input", function( ) {
		filter( );
	});
});

function getDirectories(directories, callback) {
	$.getJSON("configuration/" + directories[0] + "/configuration.json", function(data) {
		getFiles(data, function( ) {
			directories.shift( );
			
			if(directories.length > 0) getDirectories(directories, callback);
			else callback( );
		});
	});
}

function getFiles(files, callback) {
	let identifier = ( new Date( ) ).getTime( ) * Math.random( );
	
	pre_filter[identifier] = files[0];
	
	// quick launch
	pre_filter[identifier].identifier = identifier;
	
	pdfjsLib.getDocument("files/" + files[0].file).then(function(pdf) {
		return pdf.getPage(1);
	}).then(function(page) {
		let canvas = document.createElement("canvas");
		
		canvas.height = page.getViewport(1.5).height;
		canvas.width = page.getViewport(1.5).width;
			
		page.render({canvasContext: canvas.getContext("2d"), viewport: page.getViewport(1.5)}).then(function( ) {
			pre_filter[identifier].canvas = canvas.toDataURL( );
			
			files.shift( );
	
			if(files.length > 0) getFiles(files, callback);
			else callback( );
		});
	});
}

function addFile(file) {
	let template = templates.file;
	
	template = template.replace("{{ identifier }}", file.identifier);
	template = template.replace("{{ canvas }}", file.canvas);
	template = template.replace("{{ title }}", file.title);
	template = template.replace("{{ board }}", file.board);
	template = template.replace("{{ subject }}", file.subject);
	template = template.replace("{{ level }}", file.level);
	template = template.replace("{{ options }}", file.options ? file.options.join(", ") : "No options were specified for this file.");
	
	$(".files").append(template);
}

function display(post_filter) {
	let search = [ ];
	
	$.each(post_filter, function(index) {
		search.push(post_filter[index]);
	});
	
	if( $("input").val( ).length > 0 ) {
		let options = {
			threshold: 0.5,
			keys: [
				{
					name: "title", 
					weight: 0.9
				}, 
				{
					name: "board", 
					weight: 0.7
				}, 
				{
					name: "subject",
					weight: 0.5
				}, 
				{
					name: "level",
					weight: 0.3
				}, 
				{
					name: "options",
					weight: 0.1
				}
			]
		};
		
		let fuse = new Fuse(search, options);
		search = fuse.search( $("input").val( ) );
	}
	
	$.each(search, function(index) {
		addFile(search[index]);
	});
}

function filter( ) {
	$(".files").empty( );
	
	// no categories
	let post_filter = pre_filter;
	
	display(post_filter);
}